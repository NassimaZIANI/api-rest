Ce projet contient une API Rest basé en PHP pour la gestion CRUD d'un ticket pour un système de gestion d'incident.
Le ticket est constitué :
- d'un identifiant.
- d'une date.
- d'un texte décrivant le problème.
- d'une sévérité (urgent, normal, bas)

le dossier 'API_Rest_ticket' contient 5 fichiers PHP pour
- la connexion a une base de donnée nommée 'gestion_tickets' déja crée (db_ticket.php)
- la création des tickets (create_tickets.php).
- la lecture des tickets existant (read_tickets.php).
- la modification des tickets existant (update_tickets.php).
- la suppression des tickets (delete_tickets.php).