<?php
//Inclure la connexion à la BDD et la fonction de retour
include 'connect_db.php';
//Verifier si on a bien saisi l'id
if (!empty($_GET["id"])) {

//Supprimer les données ayant l'id saisi par l'utilisateur dans la table 'ticket'
	$requete = $bdd->prepare("DELETE FROM ticket WHERE idTicket=:id");
	$requete->bindParam(':id', $_GET["id"]);
	$requete->execute();

//Afficher un message si le tuple a été bien supprimé
	return_json(true, "Le tickets a été bien supprimer");
} else {
//Afficher un message d'erreur si l'utilisateur n'a pas saisi un id a supprimé
	return_json(false, "Veuillez saisir un id à supprimer");

}

?>