<?php
//Inclure la connexion à la BDD et la fonction de retour
include 'connect_db.php';
//Verifier si on a bien saisi l'id
if (!empty($_GET["id"])){
	//Verifier si on veut modifier la description et la severite
	if (!empty($_POST["severite"]) && !empty($_POST["description"])) {
		$requete = $bdd->prepare("UPDATE ticket SET description=:desc, severite=:sev WHERE idTicket=:id");
		$requete->bindParam(':sev', $_POST["severite"]);
		$requete->bindParam(':desc', $_POST["description"]);
	//Verifier si on veut modifier que la severite
	} else if(!empty($_POST["severite"])) {
		$requete = $bdd->prepare("UPDATE ticket SET severite=:sev WHERE idTicket=:id");
		$requete->bindParam(':sev', $_POST["severite"]);
	//Verifier si on veut modifier que la description
	} else if(!empty($_POST["description"])){
		$requete = $bdd->prepare("UPDATE ticket SET description=:desc WHERE idTicket=:id");
		$requete->bindParam(':desc', $_POST["description"]);
	}

	$requete->bindParam(':id', $_GET["id"]);
	$requete->execute();

	return_json(true, "Le tickets a été bien modifier");
} else {
//Afficher un message d'erreur si y a des informations manquantes
	return_json(false, "Manque des informations");

}

?>