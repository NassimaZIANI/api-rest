<?php
//Inclure la connexion à la BDD et la fonction de retour
include 'connect_db.php';

//verifier si tout les champs ont était bien saisi
if (!empty($_POST["description"]) && !empty($_POST["severite"])) {

//Inserer les données saisi par l'utilisateur dans la table 'ticket'
	$requete = $bdd->prepare("INSERT INTO ticket VALUES (NULL,CURRENT_TIME,:desc,:severite)");
	$requete->bindParam(':desc', $_POST["description"]);
	$requete->bindParam(':severite', $_POST["severite"]);
	$requete->execute();

//Afficher un message si le tuple est bien insérer
	return_json(true, "Le tickets a été bien ajouté");

} else {
//Afficher un message d'erreur si l'utilisateur n'as pas saisi tous les informations demandées
	return_json(false, "Manque des informations");

}

?>