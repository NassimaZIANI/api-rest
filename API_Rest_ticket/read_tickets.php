<?php
//Inclure la connexion à la BDD et la fonction de retour
	include 'connect_db.php';

	if(isset($_GET['id'])){
		//Selectionner dans la table ticket les tickets ayant l'id saisi par l'utilisateur
		$requete = $bdd->prepare("SELECT * FROM ticket WHERE idTicket=:id");
		$requete->bindParam(':id', $_GET["id"]);
		$requete->execute();
	} else if (isset($_GET['description'])) {
		//Selectionner dans la table ticket les tickets ayant la description saisi par l'utilisateur
		$requete = $bdd->prepare("SELECT * FROM ticket WHERE description=:desc");
		$requete->bindParam(':desc', $_GET["description"]);
		$requete->execute();
	} else if (isset($_GET['severite'])) {
		//Selectionner dans la table ticket les tickets ayant la severite saisi par l'utilisateur
		$requete = $bdd->prepare("SELECT * FROM ticket WHERE severite=:sev");
		$requete->bindParam(':sev', $_GET["severite"]);
		$requete->execute();
	} else if (isset($_GET['dateCreation'])) {
		//Selectionner dans la table ticket les tickets ayant la date saisi par l'utilisateur
		$requete = $bdd->prepare("SELECT * FROM ticket WHERE dateCreation=:dat");
		$requete->bindParam(':dat', $_GET["dateCreation"]);
		$requete->execute();
	} else {
		//Selectionner tout les tickets dans la table ticket
		$requete = $bdd->prepare("SELECT * FROM ticket");
		$requete->execute();
	}
	
	$result = $requete->fetchAll();
	//Tester si y a des tickets correspondant a notre requete
	if($requete->rowCount() > 0){

		$results["tickets"] = $result;
		//Afficher les données correspondante a notre requete
		return_json(true, "Les tickets :", $results);
	} else {
		//Afficher message dans le cas ou y a aucun ticket
		return_json(false, "Aucun ticket existant");
	}
?>