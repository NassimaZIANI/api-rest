<?php

//Ajouter un header pour dire qu'on est dans un fichier json
header('Content-Type: application/json');

//etablir une connexion à notre base de données nommée 'gestion_tickets'
try
{
	$bdd = new PDO('mysql:host=localhost;dbname=gestion_tickets;charset=utf8', "root", "");
}
//en cas d'erreur : afficher un message d'erreur
catch (Exception $e)
{
        return_json(false, "La connexion à la base de donnée a echoué");
}

//Déclaration d'une fonction pour retourner les données en format JSON
function return_json($success, $msg, $results=NULL){
	$return["success"] = $success;
    $return["msg"] = $msg;
    $return["results"] = $results;

    echo json_encode($return);
}	
?>